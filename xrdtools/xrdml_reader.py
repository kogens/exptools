import xmltodict
import numpy as np


def xrdml_file_to_data(filepath):
    with open(filepath) as f:
        xmlstring = f.read()
    xmldata = xmltodict.parse(xmlstring)
    
    return xmldata


def useful_data_from_xmldata(xmldata, start_at_zero=True):
    # Return stuff I actually need
    # Sample ID, date
    # Intensities, start and end, interpolated 2theta points
    
    sample_id = xmldata['xrdMeasurements']['sample']['id']
    scan = xmldata['xrdMeasurements']['xrdMeasurement']['scan']
    scan_timestamp = scan['header']['startTimeStamp']
    
    positions = scan['dataPoints']['positions']
    print(type(positions))
    
    if type(positions) is list:
        for position in positions:
            if position['@axis'] == '2Theta':
                (start, end) = (float(position['startPosition']),
                                float(position['endPosition']))
    else:
        (start, end) = (scan['dataPoints']['positions']['startPosition'],
                        scan['dataPoints']['positions']['endPosition'])
        (start, end) = (float(start), float(end))
    
    intensities = scan['dataPoints']['intensities']['#text']
    intensities = np.fromstring(intensities, dtype=float, sep=' ')
    if start_at_zero:
        minimum = np.min(intensities[20:-1])
        print(minimum)
        intensities -= minimum
    twotheta = np.linspace(start, end, intensities.size)
    
    scan_data = {'timestamp': scan_timestamp,
                 'id': sample_id,
                 'start': start,
                 'end': end,
                 'intensities': intensities,
                 'twotheta': twotheta}
    return scan_data


def useful_data_from_path(filepath):
    xrd_data = xrdml_file_to_data(filepath)
    scan_data = useful_data_from_xmldata(xrd_data)
    
    return scan_data


def scans_from_paths(scan_data):
    scans = {}
    for key in scan_data:
        path = scan_data[key]['path']
        scan = useful_data_from_path(path)
        
        scan_data[key] = {**scan_data[key], **scan}
    
    return scan_data
