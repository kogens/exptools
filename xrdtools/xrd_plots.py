import matplotlib.pyplot as plt
import xrdml_reader

scan_metadata = {'BM3C-F1':
                    {'path': 'xrd_data/nola_spinner_Ni-filter_4h_BM3-F1_cleaned.XRDML',
                        'extra_intensity': 0},
                 'BM3C-F5':
                    {'path': 'xrd_data/nola_spinner_Ni-filter_3h_BM3C-F5-MG28_cleaned.XRDML',
                        'extra_intensity': 1000},
                 'BM3C-F6_M1':
                     {'path': 'xrd_data/spinner_Ni-filter_4h_nola_smaller_angles_BM3C-F6-M1_corrected.XRDML',
                        'extra_intensity': 1000},
                 'BM3C-F7-M2':
                     {'path': 'xrd_data/spinner_Ni-filter_4h_nola_smaller_angles_BM3C-F7-M2_corrected.XRDML',
                        'extra_intensity': 2000}
                 }

scans = xrdml_reader.scans_from_paths(scan_metadata)

fig = plt.figure(figsize=(8, 6))
ax1 = fig.add_axes([0.1, 0.11, 0.8, 0.8])
for key in scans:
    ax1.plot(scans[key]['twotheta'], scans[key]['intensities'] + scans[key]['extra_intensity'])

ax1.set_xlim([30, 58.4])
ax1.set_ylim([-10, 7500])

ax1.xaxis.set_minor_locator(plt.MultipleLocator(1))
ax1.set_yticks([])
ax1.set_xlabel('Angle [°2θ]')
ax1.set_ylabel('Diffracted intensity [a.u.]')

y_disp = 1800  # Variable displacement depending on peak separation
ax1.text(x=52.5, y=300, s='~30wt% spinel unmixed', c='C0')
ax1.text(x=52.5, y=800 + 500, s='~20wt% spinel unmixed', c='C1')
ax1.text(x=52.5, y=1500 + 800, s='~30wt% spinel, mortar', c='C2')
ax1.text(x=52.5, y=3000 , s='66wt% spinel, mortar', c='C3')

ax1.text(x=31.2, y=1600 + y_disp, s='CoAl', horizontalalignment='center', rotation='vertical', color='gray')
ax1.text(x=35.2, y=1700 + y_disp, s='$\mathrm{Al_2O_3}$', horizontalalignment='center', rotation='vertical',
         color='gray')
ax1.text(x=36.8, y=1800 + y_disp, s='Spinel', horizontalalignment='center', rotation='vertical', color='gray')
ax1.annotate(xy=(42.7, 1400 + y_disp), s='??', xytext=(39, 1600 + y_disp), color='gray',
             arrowprops=dict(arrowstyle='-', color='gray'))
ax1.annotate(xy=(43.35, 1630 + y_disp), s='$\mathrm{Al_2O_3}$', xytext=(40, 2000 + y_disp), color='gray',
             arrowprops=dict(arrowstyle='-', color='gray'))
ax1.text(x=57.6, y=1700 + y_disp, s='$\mathrm{Al_2O_3}$', horizontalalignment='center', rotation='vertical',
         color='gray')

peak_pos = {'fcc-Co': [44.100, 51.377, 75.618],
            'Co4Al13': [43.914, 51.162, 75.271],
            'CoAl': [31.2, 43.6]}

ax2 = fig.add_axes([0.1, 0.6, 0.33, 0.3])

lightgrey = (0.8, 0.8, 0.8)
ax2.axvline(x=43.98, c=lightgrey)
ax2.axvline(x=44.15, c=lightgrey)
ax2.axvline(x=44.77, c=lightgrey)

# ax2.text(x=43.96, y=5000, s='$\mathrm{Co_4Al_{13}}$', horizontalalignment='right', c='gray')
ax2.text(x=44.05, y=160, s='$\mathrm{Co_4Al_{13}}$', rotation='vertical',
         verticalalignment='bottom', horizontalalignment='right',
         c='gray')
ax2.annotate(xy=(44.15, 4400), s='Co-fcc', xytext=(44.3, 5000), color='gray',
             arrowprops=dict(arrowstyle='-', color='gray'))
ax2.text(x=44.78, y=4300 + y_disp, s='CoAl', rotation='vertical',
         verticalalignment='bottom', horizontalalignment='left',
         c='gray')

for key in scans:
    ax2.plot(scans[key]['twotheta'], scans[key]['intensities'] + scans[key]['extra_intensity'])

ax2.set_xlim([43.55, 45.2])
ax2.set_ylim([-10, 8000])
ax2.xaxis.set_minor_locator(plt.MultipleLocator(0.2))
ax2.set_yticks([])

'''
ax3 = fig.add_axes([0.57, 0.6, 0.32, 0.3])

ax3.axvline(x=51.19, c=lightgrey)
ax3.axvline(x=51.45, c=lightgrey)
ax3.text(x=51.1, y=4200, s='$\mathrm{Co_4Al_{13}}$',
         horizontalalignment='right',
         c='gray')
ax3.text(x=51.5, y=4200, s='Co-fcc',
         horizontalalignment='left',
         c='gray')

# ax3.text(x=44.05, y=160, s='$\mathrm{Co_4Al_{13}}$', rotation='vertical',
#         verticalalignment='bottom', horizontalalignment='right',
#         c='gray')
# ax3.annotate(xy=(44.15, 4400), s='Co-fcc', xytext=(44.3, 5000), color='gray',
#             arrowprops=dict(arrowstyle='-', color='gray'))
# ax3.text(x=44.78, y=4300 + y_disp, s='CoAl', rotation='vertical',
#         verticalalignment='bottom', horizontalalignment='left',
#         c='gray')

for key in scans:
    ax3.plot(scans[key]['twotheta'], scans[key]['intensities'] + scans[key]['extra_intensity'])

ax3.set_xlim([50.3, 52.4])
ax3.set_ylim([-10, 5000])
ax3.xaxis.set_minor_locator(plt.MultipleLocator(0.2))
ax3.set_yticks([])
'''

plt.savefig('xrd_plots.png', dpi=300, bbox_inches='tight')

plt.show()

