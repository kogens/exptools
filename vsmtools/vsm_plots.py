import vsmtools
import matplotlib.pyplot as plt
import numpy as np

# plt.rc('text', usetex=True)

# vsmdata = vsmtools.VSMDataset(filepaths='testdata/2020-09-14_BM5B-66MG-F7_1100ox_after_press_run2.pkl')
# vsmdata = vsmtools.VSMDataset()
path_900C = r'C:\Users\s113053\Nextcloud\Data\VSM Mikkel\170822, Co-Al H2O 900C 72,1 mg, Trun1-6 (ProfileData)\170822, Co-Al H2O 900C 72,1 mg, Trun1-6.pkl'
path_1100C = r'C:\Users\s113053\Nextcloud\programming\exptools\vsmtools\testdata\2020-09-14_BM5B-66MG-F7_1100ox_after_press.pkl'
vsmdata900 = vsmtools.VSMDataset(filepaths=path_900C)
vsmdata1100 = vsmtools.VSMDataset(filepaths=path_1100C)
# vsmdata1100 = vsmtools.VSMDataset()
vsmdata1100.save_dataset(filepath = path_1100C)

#vsmdata.save_dataset()
vsmdata900.set_mass((72.1, 'mg'))
vsmdata1100.set_mass((15.2, 'mg'))


# vsmdata.convert_units('SI')
datalist = [vsmdata900, vsmdata1100]

fig1, ax1 = plt.subplots(2, 2, sharex=True)
for vsmdata in datalist:
    data_by_field = vsmdata.get_data_by_field(field_mT=55)
    
    for key in data_by_field:
        index = 0
        temp = data_by_field[key]['Temperature']
        # TODO implement a way to only get the highest temperature and not going down again
        T = data_by_field[key]['Temperature'][1:len(temp)-index]
        Q = data_by_field[key]['AreaPerMass'][1:len(temp)-index]
        Ms = data_by_field[key]['MagnetizationPerMass'][1:len(temp)-index]
        Hc = data_by_field[key]['Coercivity'][1:len(temp)-index]
        Mr = data_by_field[key]['Retentivity'][1:len(temp)-index]
        
        ax1[0, 0].plot(T, Q, '.-')
        ax1[0, 1].plot(T, Ms, '-o')
        ax1[1, 0].plot(T, Hc, '-o')
        ax1[1, 1].plot(T, Mr, '-o')


temp_limits = [500, 950]
Q_limits = [0, 0.8]
ax1[0,0].set_xlim(temp_limits)
ax1[0,0].set_ylabel(r'Q $\mathrm{[Am^2/kg]}$')

Ms_limits = [0, 7.5]
ax1[0,1].set_xlim(temp_limits)
#ax1[0,1].set_ylim(Ms_limits)
ax1[0,1].set_ylabel(r'$\mathrm{M_{max} \;[Am^2/kg]}$')
ax1[0,1].yaxis.set_label_position('right')
ax1[0,1].yaxis.tick_right()

Hc_limits = [0, 7]
ax1[1,0].set_xlim(temp_limits)
#ax1[1,0].set_ylim(Hc_limits)
ax1[1,0].set_ylabel('Hc [A/m]')
ax1[1,0].set_xlabel('T [C]')

Mr_limits = [0, 0.004]
ax1[1,1].set_xlim(temp_limits)
#ax1[1,1].set_ylim(Mr_limits)
ax1[1,1].set_ylabel('Mr $\mathrm{[Am^2/kg]}$')
ax1[1,1].yaxis.set_label_position('right')
ax1[1,1].yaxis.tick_right()
ax1[1,1].set_xlabel('T [C]')

#plt.show()

"""
curiekeys900 = ['170822, Co-Al H2O 900C 72,1 mg, Trun1-6#4.dat',
                '170822, Co-Al H2O 900C 72,1 mg, Trun1-6#265.dat',
                '170822, Co-Al H2O 900C 72,1 mg, Trun1-6#525.dat',
                '170822, Co-Al H2O 900C 72,1 mg, Trun1-6#785.dat']
curiedata = vsmdata900.get_curie_experiments()
curiedata900 = {}
for key in curiedata:
    if key in curiekeys900:
        curiedata900[key] = curiedata[key]

curiekeys1100 = ['2020-09-14_BM5B-66MG-F7_1100ox_after_press#74.dat',
                 '2020-09-14_BM5B-66MG-F7_1100ox_after_press#82.dat',
                 '2020-09-14_BM5B-66MG-F7_1100ox_after_press#90.dat']
curiedata = vsmdata1100.get_curie_experiments()
curiedata1100 = {}
for key in curiedata:
    if key in curiekeys1100:
        curiedata1100[key] = curiedata[key]

# fig, ax = plt.figure(2)
mass900 = 72.1*1E-6
mass1100 = 15.2*1E-6

T900 = curiedata900['170822, Co-Al H2O 900C 72,1 mg, Trun1-6#265.dat']['Temperature Data']
M900 = curiedata900['170822, Co-Al H2O 900C 72,1 mg, Trun1-6#265.dat']['MomentX Data']/mass900


T1100 = curiedata1100[curiekeys1100[0]]['Temperature Data']
M1100 = curiedata1100[curiekeys1100[0]]['MomentX Data']/mass1100

fig2, ax2 = plt.subplots(1,1)
ax2.plot(T900, M900)
ax2.plot(T1100, M1100)


ax2.set_xlabel('T [C]')
ax2.set_ylabel('$\mathrm{\sigma \; [Am^2/kg]}$')
"""
plt.show()