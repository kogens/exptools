# Color coding for VSM log and profiles in Notepad++
These files define custom colorcoding for Lakeshore VSM log files (\*_Log.txt) and Profile files (\*.pfl)


**How to**

Place the files in `%APPDATA%\Notepad++\userDefineLangs`. It should automatically recognize \*.pfl files, 
but for log files you need to set it yourself after opening the file (they just end in .txt which is too broad).
After opening the file, choose "Language" in the top menubar and select the either "VSM log" or "VSM profile".

A VSM profile should then look like this:

![Color coded .pfl file](docs/profile_example.png)

and the log like this:

![Color coded logfile](docs/logfile_example.png)