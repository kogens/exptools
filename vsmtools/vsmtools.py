import re
import os
import pickle
import numpy as np
from tkinter import Tk
from tkinter.filedialog import askopenfilename, askopenfilenames, asksaveasfile
import csv
import warnings
import yaml
import time


class VSMDataset:
    def __init__(self, filepaths=None, unit_system='SI', sample_name='', mass=(0, None)):
        self.sample_name = sample_name
        self.mass = mass[0]
        self.mass_unit = mass[1]

        if self.mass is not 0:
            self.set_mass(mass)

        self.dataset = None
        self.load_files(filepaths)
        self.unit_system = None
        self.convert_units(unit_system)

        self.data_by_field = None

        return
    
    def convert_units(self, unit_system='SI'):
        self.dataset = convert_units(self.dataset, unit_system)
        self.unit_system = unit_system
        return
    
    def set_sample_parameters(self, mass=None, sample_name=''):
        self.sample_name = sample_name
        self.mass = mass

    def mass_in_kg(self):
        mass_units = {'mg': 1E-6, 'g': 1E-3, 'kg': 1}
        if self.mass_unit in mass_units.keys():
            mass_kg = self.mass * mass_units[self.mass_unit]
        else:
            mass_kg = None
        return mass_kg
    
    def set_mass(self, mass, mass_unit='mg'):
        mass_units = {'mg': 1E-6, 'g': 1E-3, 'kg': 1}

        if type(mass) == float or type(mass) == int:
            if mass_unit in mass_units:
                self.mass = mass
                self.mass_unit = mass_unit
            else:
                warnings.warn('Incompatible mass unit given')
        elif type(mass) is tuple:
            mass_unit = mass[1]
            if mass_unit in mass_units:
                self.mass = mass[0]
                self.mass_unit = mass[1]
            else:
                warnings.warn('Incompatible mass unit given')

    def load_files(self, filetype=None, filenames=None):
        data = load_files(filetype, filenames)
        self.dataset = data['Dataset']

    def save_dataset(self, filepath=None):
        save_dataset(self.dataset, filepath)
    
    def get_data_by_field(self, field_mT='all'):
        """ Examples
        field_mT = 'all'
        field_mT = 500
        field_mT = (20, 1500) --> return all between 20 and 1500 mT
        field_mT = [20, 550, 1500] --> return 20, 55 and 1500 mT values if found
        """
        
        if self.data_by_field is None:
            self.calculate_data_by_field()
        data_by_field = self.data_by_field
        data = {}
        
        if field_mT == 'all':
            return data_by_field
        elif (type(field_mT) is int) or (type(field_mT) is float):
            for key in data_by_field:
                max_field = data_by_field[key]['MaxField']
                max_field_unit = data_by_field[key]['AppliedFieldUnit']
                
                if max_field == self.field_conversion(field_mT, 'mT', max_field_unit):
                    data[key] = data_by_field[key]
                    
        elif type(field_mT) is tuple:
            for key in data_by_field:
                max_field = data_by_field[key]['MaxField']
                max_field_unit = data_by_field[key]['AppliedFieldUnit']
                
                for field in field_mT:
                    if max_field == self.field_conversion(field, 'mT', max_field_unit):
                        data[key] = data_by_field[key]
                        
        elif type(field_mT) is list:
            for key in data_by_field:
                max_field = data_by_field[key]['MaxField']
                max_field_unit = data_by_field[key]['AppliedFieldUnit']
                
                field_lower = self.field_conversion(field_mT[0], 'mT', max_field_unit)
                field_upper = self.field_conversion(field_mT[1], 'mT', max_field_unit)
                
                if (max_field >= field_lower) and (max_field <= field_upper):
                        data[key] = self.data_by_field[key]
        
        return data
    
    
    def field_conversion(self, field, from_unit, to_unit):
        field_units = {'µT': 1e-6, 'muT': 1e-6, 'mT': 1e-3, 'T': 1}
        
        if (from_unit in field_units) and (to_unit in field_units):
            field = field * field_units[from_unit] / field_units[to_unit]
        else:
            warnings.warn('Conversion unit not found. Allowed units: %s' %(field_units.keys()))
            return None
            
        return field
        
        
    def Q_vs_T_by_field(self, field_mT='all'):
        data_by_field = self.get_data_by_field(field_mT)
        
        #Q = self.data_by_field[]
        
        return None

    def Hc_vs_T_by_field(self):
        return None

    def Mr_vs_T_by_field(self):
        return None

    def calculate_data_by_field(self, field_display_unit='mT'):
        # TODO group experiments by times, if e.g. they are run multiple times
        # Probably need to steal the experiment time from the .txt files, as the .dat file does not have it
        data_by_field = {}
        dataset_info = {}
        highest_temp = 0
        skip = False
        for datfile, datadict in self.dataset.items():
            experiment_type = datadict['Experiment']
            if experiment_type == 'Hysteresis' and not skip:
                max_field = datadict['MaxField']
                # display_field = max_field / field_conversion
                applied_field_unit = datadict['AppliedFieldUnit']
                display_field = self.field_conversion(max_field, applied_field_unit, field_display_unit)
                moment_unit = datadict['MomentUnit']
                area_unit = moment_unit + applied_field_unit
                unit_system = datadict['System']

                temperature = np.median(datadict['Temperature Data'])
                if temperature + 10 > highest_temp:
                    highest_temp = temperature
                else:
                    skip = True
                    
                saturation_magnetization = datadict['Magnetization']
                retentivity = datadict['Retentivity']
                hysteresis_area = datadict['TotalArea']
                coercivity = datadict['Coercivity']

                if self.mass is None:
                    warnings.warn('No mass given for sample, Q set to 1')
                    area_per_mass = 0
                    magnetization_per_mass = 0
                    retentivity_per_mass = 0
                    mass_kg = 0
                    mass_unit = 'mg'
                else:
                    mass_kg = self.mass_in_kg()
                    mass_unit = 'kg'
                    area_per_mass = hysteresis_area/mass_kg
                    magnetization_per_mass = saturation_magnetization/mass_kg
                    retentivity_per_mass = retentivity/mass_kg
                
                area_per_mass_unit = area_unit + '/' + mass_unit

                # print('%.2f, %.4f' %(temperature, area_per_mass))
                # TODO rewrite to consider values close to each other instead of just round
                # Rounds to 0 decimals, if e.g. unit is in T everything is just 0 and info is not saved.
                # Number experiments by when they are obtained
                key = str(round(display_field)) + ' ' + field_display_unit
                
                if key in data_by_field:
                    data_by_field[key]['Temperature'].append(temperature)
                    data_by_field[key]['Magnetization'].append(saturation_magnetization)
                    data_by_field[key]['MagnetizationPerMass'].append(magnetization_per_mass)
                    data_by_field[key]['Retentivity'].append(retentivity)
                    data_by_field[key]['RetentivityPerMass'].append(retentivity_per_mass)
                    data_by_field[key]['Coercivity'].append(coercivity)
                    data_by_field[key]['Area'].append(hysteresis_area)
                    data_by_field[key]['AreaPerMass'].append(area_per_mass)
                else:
                    data_by_field[key] = {'System': unit_system,
                                          'MomentUnit': moment_unit,
                                          'AppliedFieldUnit': applied_field_unit,
                                          'AreaUnit': area_unit,
                                          'AreaPerMassUnit': area_per_mass_unit,
                                          'MaxField': max_field,
                                          'Temperature': [temperature],
                                          'Magnetization': [saturation_magnetization],
                                          'MagnetizationPerMass': [magnetization_per_mass],
                                          'Retentivity': [retentivity],
                                          'RetentivityPerMass': [retentivity_per_mass],
                                          'Coercivity': [coercivity],
                                          'Area': [hysteresis_area],
                                          'AreaPerMass': [area_per_mass]
                                          }
        
        for key in data_by_field:
            data_by_field[key]['Temperature'] = np.array(data_by_field[key]['Temperature'])
            data_by_field[key]['Magnetization']= np.array(data_by_field[key]['Magnetization'])
            data_by_field[key]['MagnetizationPerMass']= np.array(data_by_field[key]['MagnetizationPerMass'])
            data_by_field[key]['Retentivity'] = np.array(data_by_field[key]['Retentivity'])
            data_by_field[key]['RetentivityPerMass'] = np.array(data_by_field[key]['RetentivityPerMass'])
            data_by_field[key]['Coercivity']= np.array(data_by_field[key]['Coercivity'])
            data_by_field[key]['Area']= np.array(data_by_field[key]['Area'])
            data_by_field[key]['AreaPerMass']= np.array(data_by_field[key]['AreaPerMass'])
            
        dataset_info = {'System': unit_system,
                     'MomentUnit': moment_unit,
                     'AppliedFieldUnit': applied_field_unit,
                      'AreaUnit': area_unit,
                     'AreaPerMassUnit': area_per_mass_unit,
                     'MaxField': max_field,
                     'Mass': str(self.mass_in_kg()) + ' kg'}
        
        # data_by_field['DataInfo'] = dataset_info
        
        self.data_by_field = data_by_field
        
        return data_by_field
    
    def get_dataset(self):
        return self.dataset
    
    def get_curie_experiments(self):
        # Return datasets containing Curie measurements
        curieexperiments = {}
        for key in self.dataset:
            try:
                if self.dataset[key]['Experiment'] == 'TimeExp':
                    curieexperiments[key] = self.dataset[key]
            except KeyError:
                continue
        
        self.curie_experiments = curieexperiments
                
        return curieexperiments

    def get_hysteresis_experiments(self):
        # Return datasets containing Curie measurements
        return
    
    def get_dataset_info(self):
        dataset_info = {'SampleName': self.sample_name,
                        'Mass': self.mass,
                        'MassUnit': self.mass_unit,
                        'UnitSystem': self.unit_system}
        return dataset_info


def load_files(filepaths=None, filetype=None):
    if filepaths is None:
        Tk().withdraw()  # Keep the root window from appearing
        if filetype is None:
            filepaths = askopenfilenames(title='Choose .dat files',
                                         filetypes=[('VSM data files', '*.dat'),
                                                    ('Python pickle files', '*.pkl')])
        elif filetype == 'dat':
            filepaths = askopenfilenames(title='Choose .dat files', filetypes=[('VSM data files', '*.dat')])
        elif filetype.lower() == 'pkl' or filetype.lower() == 'pickle':
            filepaths = askopenfilenames(title='Choose pickle files', filetypes=[('Pickled VSM files', '*.pkl')])
        else:
            warnings.warn('Only .dat or .pkl files supported for now. Data not loaded.')
            return None

    dataset = {}
    if type(filepaths) is str: filepaths = [filepaths] # Workaround so we don't loop through letters in a string
    start = time.perf_counter()
    for file in filepaths:
        filename = os.path.basename(file)
        _, file_extension = os.path.splitext(file)
        if file_extension == '.dat':
            data = datfile_to_dict(file)
            dataset[filename] = data
        elif file_extension == '.pkl':
            dataset = load_pickled_dataset(file)
        else:
            warnings.warn('Only .dat or .pkl files supported for now. Data not loaded.')
            raise IOError
    stop = time.perf_counter()
    print('Total loading times of %i files: %f' %(len(filepaths), stop-start))
    
    return {'Dataset': dataset}


def load_pickled_dataset(filepath=None):
    if filepath is None:
        Tk().withdraw()
        filepath = askopenfilename(title='Choose pickled dataset', filetypes=[('Python pickle', '*.pkl')])
    try:
        with open(filepath, 'rb') as f:
            dataset = pickle.load(f)
    except IOError as e:
        warnings.warn('Could not import pickle file. Is the filetype correct?')
        raise e
    
    return dataset


def save_dataset(dataset, filepath=None):
    if filepath is None:
        Tk().withdraw()
        savefile = asksaveasfile(mode='w', filetypes=[('Python pickle', '*.pkl')], defaultextension='.pkl')
        if savefile is None:  # If save is canceled
            warnings.warn('No savefile chosen, data was not saved!')
            return
        filepath = savefile.name
    
    try:
        with open(filepath, 'wb') as f:
            pickle.dump(dataset, f)
    except IOError as e:
        warnings.warn('Error pickling file, data not exported!\n' + filepath)
        warnings.warn(e)
    
    return


def datfile_to_dict(filepath):
    _, file_extension = os.path.splitext(filepath)
    if file_extension == '.dat':
        try:
            with open(filepath, 'r') as f:
                file_contents = f.read()
        except IOError as e:
            warnings.warn('Could not read datfile')
            raise e
    else:
        warnings.warn('Wrong filetype, make sure a .dat file is chosen')
        raise FileNotFoundError

    # Match stuff like 'Experiment TimeExp' or 'TimeConstant 0.1'
    regex_oneliners = re.compile(r'^(\S+) (\S*)$', re.MULTILINE)

    # Match multiline data, e.g. 'VertexField\n100\n100' and the moment data
    regex_multiline_data = re.compile(r'^([\S ]+)\n((?:[-.E\d]+\n)+)', re.MULTILINE)

    oneliners = re.findall(regex_oneliners, file_contents)
    multiline_data = re.findall(regex_multiline_data, file_contents)

    start1 = time.perf_counter()
    datadict = {}
    for match in oneliners:
        try:
            datadict[match[0]] = float(match[1])
        except ValueError:
            datadict[match[0]] = match[1]
    stop1 = time.perf_counter()

    start2 = time.perf_counter()
    for match in multiline_data:
        data = match[1]
        #data_split = [float(n) for n in data.splitlines()]
        data_split = list(map(float, data.splitlines()))
        datadict[match[0]] = np.array(data_split)

    stop2 = time.perf_counter()

    # print('Matching time: %f' % (stop1 - start1))
    # print('Dict creation time: %f' % (stop2 - start2))

    return datadict


# TODO
# Export a datapoints as simple file that is easy to plot in e.g. Excel
def save_csv(dataset, plot_types='all'):
    filename = 'test.csv'
    #qvst = extract_QvsT_by_field(dataset)
    
    with open(filename, 'w') as f:
        fieldnames = ['T (C)', 'Q (J/kg)']
        writer = csv.writer(f)
        writer.writerow(fieldnames)
        writer.writerows(zip(*([1, 2, 3, 4], [5, 6, 7, 8])))


def load_unit_conversions(config_file='units.yml'):
    try:
        with open(config_file) as yamlfile:
            conversions = yaml.safe_load(yamlfile)
    except FileNotFoundError as e:
        warnings.warn('Unit conversion file %s not found' %(config_file))
        raise e
    
    return conversions


def convert_units(dataset, unit_system='SI'):
    conversions = load_unit_conversions()
    
    for datakey in dataset:
        data = dataset[datakey]
        if (data['System'].upper() == 'CGS') and (unit_system.upper() == 'SI'):
            data['System'] = 'SI'
            data['MomentUnit'] = 'Am^2'
            data['AppliedFieldUnit'] = 'T'
            field_conversion = float(conversions['ConversionFactors']['CGStoSI']['Field'])
            moment_conversion = float(conversions['ConversionFactors']['CGStoSI']['Moment'])
            area_conversion = float(conversions['ConversionFactors']['CGStoSI']['Area'])
        elif (data['System'].upper() == 'SI') and (unit_system.upper == 'CGS'):
            data['System'] = 'CGS'
            data['MomentUnit'] = 'emu'
            data['AppliedFieldUnit'] = 'Oe'
            field_conversion = float(conversions['ConversionFactors']['SItoCGS']['Field'])
            moment_conversion = float(conversions['ConversionFactors']['SItoCGS']['Moment'])
            area_conversion = float(conversions['ConversionFactors']['SItoCGS']['Area'])
        elif data['System'].upper() == unit_system.upper():
            field_conversion = 1
            moment_conversion = 1
            area_conversion = 1
        else:
            field_conversion = 1
            moment_conversion = 1
            area_conversion = 1
            warnings.warn('Unit conversion aborted for %s, could not detect which unit to convert' %(datakey))
            return
            
        for key in data:
            if key in conversions['DataTypes']['Field']:
                data[key] = data[key] * field_conversion
            elif key in conversions['DataTypes']['Moment']:
                data[key] = data[key] * moment_conversion
            elif key in conversions['DataTypes']['Area']:
                data[key] = data[key] * area_conversion
    
    return dataset


def find_runs(dataset):
    """ Characterise multiple "runs" of the sample and whether temperature is increasing,
        decreasing or constant during experiment.
        Returns (run, temperature_change) where run is integer and temperature_change is string;
        "increasing", "decreasing", "constant" (within limit) or "unknown"
    """
    
    for data in dataset.values():
        print(data['Experiment'] + ' '+  str(np.std(data['Temperature Data'])))
    
    
    
    
    return
    

if __name__ == '__main__':
    filedir = r'C:\Users\s113053\Nextcloud\Data\VSM insitu\2020-09-14_BM5B-66MG-F7_1100ox_after_press (ProfileData)\\'
    '''
    files = [filedir + '2020-09-14_BM5B-66MG-F7_1100ox_after_press#1.dat',
             filedir + '2020-09-14_BM5B-66MG-F7_1100ox_after_press#2.dat',
             filedir + '2020-09-14_BM5B-66MG-F7_1100ox_after_press#3.dat',
             filedir + '2020-09-14_BM5B-66MG-F7_1100ox_after_press#4.dat',
             filedir + '2020-09-14_BM5B-66MG-F7_1100ox_after_press#5.dat']
    dataset = load_files(files)
    dataset_pkl = load_files(filedir + '2020-09-14_BM5B-66MG-F7_1100ox_after_press.pkl')
    #print('test')
    '''
    vsmdata = VSMDataset(filepaths='testdata/2020-09-14_BM5B-66MG-F7_1100ox_after_press_run2.pkl')
    #vsmdata = VSMDataset()
    vsmdata.save_dataset(filepath='testdata/2020-09-14_BM5B-66MG-F7_1100ox_after_press.pkl')
    vsmdata.set_mass((15.2, 'mg'))
    QvsT = vsmdata.get_data_by_field(field_mT=[500, 1500])
    curieexp = vsmdata.get_curie_experiments()
    
    exptype = find_runs(vsmdata.dataset)
    
    print('converted!')