import re
import warnings
import matplotlib.pyplot as plt
from tkinter import Tk
from tkinter.filedialog import askopenfilename, askopenfilenames, asksaveasfile


class ProfileReader:
    def __init__(self):
        return
    
    def load_profile(self, filepath=None):
        if filepath is None:
            Tk().withdraw()  # Keep root window from appearing
            filepath = askopenfilename(title='Choose profile file', filetypes=[('VSM profile', '*.pfl')])
        
        try:
            with open(filepath, 'r') as f:
                profile = f.readlines()
        except FileNotFoundError as e:
            raise e
        
        experiments = []
        ramps = []
        
        t = 0
        T = 20
        time = []
        temp = []
        
        for line in profile:
            line = line.replace('"', '')
            line = line.rstrip()
            line = line.split(',')
            
            if line[0].lower() == 'exp':
                time.append(t)
                temp.append(T)
                t += 1
            elif line[0].lower() == 'ramp':
                time.append(t)
                temp.append(T)
                t += 2
                T = float(line[2])
        
        plt.plot(time, temp)
        plt.show()
        return


if __name__ == '__main__':
    reader = ProfileReader()
    reader.load_profile(filepath=r'C:\Users\s113053\Nextcloud\programming\exptools\vsmtools\Master_22_reduced_sampling_4xTc.pfl')
    
    